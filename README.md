# easy-password

A Python script that makes passwords that are hopefully easy to remember. The help output is below.

```
usage: ./easy_password.py [-h] [-w WORD_COUNT] [-c CHAR_COUNT] [-p PREFIX] [-s SUFFIX] [-d] [-a]

A script that generates secure passwords that are hopefully memorable.

optional arguments:
  -h, --help            show this help message and exit
  -w WORD_COUNT, --word-count WORD_COUNT
                        The number of words required for the password. (default: 5)
  -c CHAR_COUNT, --char-count CHAR_COUNT
                        The number of characters required for the password. (default: 6)
  -p PREFIX, --prefix PREFIX
                        Specifies a prefix required for the words in the password. (default: '')
  -s SUFFIX, --suffix SUFFIX
                        Specifies a suffix required for the words in the password. (default: '')
  -d, --display-entropy
                        Print the bits of entropy on the line after the password. (default: False)
  -a, --allow-caps      Allow words to be capitalized. (default: False)
```