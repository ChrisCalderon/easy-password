#!/usr/bin/env python3 -O
# easy-password
# Copyright (C) 2021  Christian Calderon
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Contact the author via email at calderonchristian73 AT gmail DOT com.

"""A script that generates secure passwords that are hopefully memorable."""
from random import SystemRandom
from argparse import ArgumentParser
from typing import List
from dataclasses import dataclass, field, fields
from math import log2
from warnings import warn
import sys

WORDS = '/usr/share/dict/words'
PROJ = 'easy-password'
PROG = 'easy_password.py'
VERSION = '1', '0', '1'


@dataclass(frozen=True)
class ArgVals:
    word_count: int = field(
        default=5,
        metadata={
            'help': 'The number of words required for the password.'
        }
    )
    char_count: int = field(
        default=6,
        metadata={
            'help': 'The number of characters required for the password.'
        }
    )
    prefix: str = field(
        default='',
        metadata={
            'help': 'Specifies a prefix required for the words in the password.'
        }
    )
    suffix: str = field(
        default='',
        metadata={
            'help': 'Specifies a suffix required for the words in the password.'
        }
    )
    display_entropy: bool = field(
        default=False,
        metadata={
            'help': 'Print the bits of entropy on the line before the password.'
        }
    )
    allow_caps: bool = field(
        default=False,
        metadata={
            'help': 'Allow words to be capitalized.'
        }
    )
    version: bool = field(
        default=False,
        metadata={
            'help': 'Print version info and exit.'
        }
    )


def parse_args(argv: List[str]) -> ArgVals:
    parser = ArgumentParser(
        prog=PROG,
        description=__doc__,
    )
    for f in fields(ArgVals):
        args = (
            '-{}'.format(f.name[0]),
            '--{}'.format(f.name.replace('_', '-')),
        )
        
        if f.type is bool:
            # store_false/true actions don't support type keyword.
            kwds = {
                'default': f.default,
                'action': 'store_false' if f.default else 'store_true',
                'help': f.metadata['help']
            }
        else:
            kwds = {
                'type': f.type,
                'default': f.default,
                'help': f.metadata['help'] + ' (default: {!r:})'.format(f.default)
            }

        parser.add_argument(*args, **kwds)
    
    args = parser.parse_args(argv[1:])
    return ArgVals(**vars(args))


def print_version():
    info = '''{} {}
Copyright (C) 2021 Christian Calderon
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.'''
    print(info.format(PROJ, '.'.join(VERSION)))


def main():
    if sys.version_info < (3, 8):
        warn('This script is untested with Python versions older than 3.8!', RuntimeWarning)
    
    args = parse_args(sys.argv)

    if args.version:
        print_version()
        return
        
    rng = SystemRandom()

    with open(WORDS) as f:
        all_words = map(str.strip, f)
        subset = [w for w in all_words if
                  len(w) == args.char_count and
                  w.startswith(args.prefix) and
                  w.endswith(args.suffix) and
                  ((not w.istitle()) or args.allow_caps)]

    if __debug__ or args.display_entropy:
        print('~{} bits of entropy.'.format(int(args.word_count * log2(len(subset)))))

    print(' '.join(rng.choices(subset, k=args.word_count)))


if __name__ == '__main__':
    main()
